package scripts.mWineGrabber.user;


import org.tribot.api.General;
import org.tribot.api2007.types.RSArea;
import org.tribot.api2007.types.RSTile;
import org.tribot.script.Script;
import scripts.api.Calculations;
import scripts.api.types.Spell;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Jeroen on 12-8-2015.
 */
public class Vars {
    private static Vars vars;
    public long START_TIME = System.currentTimeMillis();

    public Script script;
    public boolean stopping = false;
    public long last_busy_time = System.currentTimeMillis();
    public String STATE = "INIT";

    public boolean gui_filled = false;
    public boolean gui_hop = false;
    public boolean gui_ingame = false;
    public boolean gui_rand_order = false;
    public boolean gui_alch = false;

    public HashMap<String,Integer> itemToAlch = new HashMap<>();
    public int highAlchCasts = 0;
    public int telegrabCasts = 0;
    public int faladorCasts = 0;


    public int winesGrabbed = 0;
    public boolean grabbedWine = false;
    public boolean lost  = false;

    public long time_exceed_login_limit = 0;
    public int TIME_BETWEEN_TOO_FAST_HOPPING = 60000;

    public ArrayList<Integer> worlds = new ArrayList<Integer>();
    public String itemName = "";
    public HashMap<String,Integer> requiredItems = new HashMap<>();


    //Area's
    public RSArea temple = new RSArea(new RSTile(2936, 3517, 0),new RSTile(2930, 3513, 0));
    public RSArea falador = new RSArea(new RSTile(2961, 3385, 0),new RSTile(2969, 3376, 0));
    //Tile's
    public RSTile castSpot = new RSTile(2931, 3515, 0);


    public static Vars get() {
        return vars == null ? vars = new Vars() : vars;
    }

    public static void reset() {
        vars = null;
    }
}
