package scripts.mWineGrabber.user;

import org.tribot.api2007.types.RSTile;

/**
 * Created by Jeroen on 31-8-2015.
 */
public class Paths {

    public static final RSTile[] pathTemple = {
            new RSTile(2946, 3368, 0),
            new RSTile(2946, 3374, 0),
            new RSTile(2951, 3378, 0),
            new RSTile(2957, 3381, 0),
            new RSTile(2961, 3386, 0),
            new RSTile(2965, 3392, 0),
            new RSTile(2966, 3398, 0),
            new RSTile(2966, 3404, 0),
            new RSTile(2966, 3410, 0),
            new RSTile(2962, 3415, 0),
            new RSTile(2956, 3418, 0),
            new RSTile(2953, 3424, 0),
            new RSTile(2949, 3429, 0),
            new RSTile(2948, 3435, 0),
            new RSTile(2948, 3441, 0),
//            new RSTile(2964, 3405, 0),
            new RSTile(2948, 3445, 0),
            new RSTile(2948, 3451, 0),
            new RSTile(2946, 3457, 0),
            new RSTile(2945, 3463, 0),
            new RSTile(2945, 3469, 0),
            new RSTile(2945, 3475, 0),
            new RSTile(2945, 3481, 0),
//            new RSTile(2952, 3443, 0),
            new RSTile(2944, 3483, 0),
            new RSTile(2943, 3489, 0),
            new RSTile(2942, 3495, 0),
            new RSTile(2941, 3501, 0),
            new RSTile(2941, 3507, 0),
            new RSTile(2941, 3513, 0),
            new RSTile(2936, 3517, 0),
            new RSTile(2932, 3515, 0)
    };

    public static final RSTile[] pathBank = {
            new RSTile(2962, 3381, 0),
            new RSTile(2956, 3381, 0),
            new RSTile(2950, 3377, 0),
            new RSTile(2946, 3372, 0),
            new RSTile(2946, 3368, 0)
    };

    public static final RSTile[] escapePath = {
            new RSTile(2931, 3515, 0),
            new RSTile(2937, 3516, 0),
            new RSTile(2942, 3512, 0),
            new RSTile(2941, 3506, 0),
            new RSTile(2941, 3500, 0),
            new RSTile(2942, 3494, 0)
    };
}
