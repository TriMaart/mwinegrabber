package scripts.mWineGrabber;

import org.tribot.api.General;
import org.tribot.api.Timing;
import org.tribot.api.types.generic.Condition;
import org.tribot.api2007.Banking;

import org.tribot.api2007.Game;
import org.tribot.api2007.Login;
import org.tribot.api2007.Skills;
import org.tribot.script.Script;

import org.tribot.script.ScriptManifest;
import org.tribot.script.interfaces.MessageListening07;
import org.tribot.script.interfaces.Painting;
import org.tribot.script.interfaces.Starting;
import scripts.api.Calculations;
import scripts.api.InventoryListener.InventoryListener;
import scripts.api.InventoryListener.InventoryObserver;
import scripts.api.Node;

import scripts.api.types.Spell;
import scripts.mWineGrabber.nodes.*;
import scripts.mWineGrabber.user.Vars;

import java.awt.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

/**
 * Created by Jeroen on 31-8-2015.
 */
@ScriptManifest(authors = {"maart"}, name = "mWineGrabber", category = "Money Making", description = "Tele grabs wine of zamoraks, capable of world hopping and high alching whilst doing so.")
public class mWineGrabber extends Script implements InventoryListener, Painting, Starting, MessageListening07 {

    private int startXP;
    private int startingLevel;

    private final int	PAINT_X		= 370;
    private final int	PAINT_BOT_Y	= 200;
    private final int	PAINT_SPACE	= 15;

    public static ArrayList<Node> nodes = new ArrayList<Node>();


    @Override
    public void run() {
        while (Login.getLoginState() != Login.STATE.INGAME || !Vars.get().gui_filled) {
            sleep(250);
        }
        INIT();
        Collections.addAll(nodes, new AB(),new WalkTemple(),new GrabWine(), new WorldHop(), new TooFast(), new Teleport(),new Bank(), new RunFromCombat());
        loop(200, 400);
        Login.logout();
    }

    private void loop(int min, int max) {
        while (!Vars.get().stopping) {
            for (final Node node : nodes) {    //loop through the nodes

                if (node.validate()) {     //validate each node
                    node.execute();    //execute
                    sleep(General.random(min, max));    //time in between executing nodes
                }
            }
        }
    }

    @Override
    public void inventoryItemGained(int id, int count) {
        if (id == 245){
            Vars.get().winesGrabbed++;
        }
    }

    @Override
    public void inventoryItemLost(int id, int count) {

    }

    private void INIT(){
        Vars.get().requiredItems = getRequiredHashMap();
    }

    private HashMap<String,Integer> getRequiredHashMap(){
        HashMap<String,Integer>  hash  = new HashMap<>();
        hash.putAll(Calculations.mergeAndAdd(Spell.FALADOR_TELEPORT.getRunes(Vars.get().faladorCasts), Spell.TELEKINECTIC_GRAB.getRunes(Vars.get().telegrabCasts)));
        if (Vars.get().gui_alch){
            hash.putAll(Calculations.mergeAndAdd(Spell.HIGH_LEVEL_ALCHEMY.getRunes(Vars.get().highAlchCasts), Vars.get().itemToAlch));
        }
        return hash;
    }


    public void onStart() {
        new Gui().setVisible(true);
        General.useAntiBanCompliance(true);
        startXP = Skills.getXP(Skills.SKILLS.MAGIC);
        startingLevel = Skills.getActualLevel(Skills.SKILLS.MAGIC);

        Vars.reset();
        Vars.get().script = this;

        InventoryObserver inventoryObserver = new InventoryObserver(new Condition() {
            @Override
            public boolean active() {
                return !Banking.isBankScreenOpen();
            }
        });
        inventoryObserver.addListener(this);
        inventoryObserver.start();
    }


    @Override
    public void onPaint(Graphics g) {
        int level = Skills.getActualLevel(Skills.SKILLS.MAGIC);
        int xpGained = Skills.getXP(Skills.SKILLS.MAGIC) - startXP;

        g.drawString("State: "+Vars.get().STATE,PAINT_X,PAINT_BOT_Y);
        g.drawString("Time running: "+Calculations.getTimeRan(Vars.get().START_TIME),PAINT_X,PAINT_BOT_Y+PAINT_SPACE);
        g.drawString("Current level (gained): " + level + " (" + (level - startingLevel) + ")",PAINT_X,PAINT_BOT_Y+(PAINT_SPACE*2));
        g.drawString("XP gained (p/h): " + xpGained + " (" + Calculations.getPerHour(xpGained, Vars.get().START_TIME) + ")",PAINT_X,PAINT_BOT_Y+(PAINT_SPACE*3));
        g.drawString("Wines grabbed: "+Vars.get().winesGrabbed + " (" + Calculations.getPerHour(Vars.get().winesGrabbed,Vars.get().START_TIME) + ")",PAINT_X,PAINT_BOT_Y+(PAINT_SPACE*4));


    }

    @Override
    public void clanMessageReceived(String s, String s1) {

    }

    @Override
    public void serverMessageReceived(String s) {
        if (s.equals("Too late - it's gone!")){
            General.println("LOST ONE");
            if (Vars.get().gui_hop && Timing.timeFromMark(Vars.get().time_exceed_login_limit) > Vars.get().TIME_BETWEEN_TOO_FAST_HOPPING) {
                Vars.get().lost = true;
            }
        }
    }

    @Override
    public void tradeRequestReceived(String s) {

    }

    @Override
    public void personalMessageReceived(String s, String s1) {

    }

    @Override
    public void duelRequestReceived(String s, String s1) {

    }

    @Override
    public void playerMessageReceived(String s, String s1) {

    }
}
