package scripts.mWineGrabber.nodes;

import org.tribot.api.General;
import org.tribot.api.Timing;
import org.tribot.api.types.generic.Condition;
import org.tribot.api2007.Game;
import org.tribot.api2007.Inventory;
import org.tribot.api2007.Player;
import org.tribot.api2007.Walking;
import scripts.api.AntiBan;
import scripts.api.Calculations;
import scripts.api.Node;
import scripts.api.mWalking;
import scripts.mWineGrabber.user.Paths;
import scripts.mWineGrabber.user.Vars;

/**
 * Created by Jeroen on 31-8-2015.
 */
public class WalkTemple extends Node {
    @Override
    public void execute() {
        Vars.get().STATE = "Walking to the temple.";
        AntiBan.activateRun();
        //check if you are in the temple.
        if (Vars.get().temple.contains(Player.getPosition())){
            //check if you are on the cast spot
            while (!Player.getPosition().equals(Vars.get().castSpot)){
                //walk to the cast spot.
                if (Vars.get().castSpot.isOnScreen()){
                    Walking.clickTileMS(Vars.get().castSpot,"Walk");
                }
                else {
                    Walking.walkTo(Vars.get().castSpot);
                }
                //wait before clicking again.
                General.sleep(1000,500);
                Timing.waitCondition(new Condition() {
                    @Override
                    public boolean active() {
                        return !Player.isMoving();
                    }
                }, General.random(3000,4000));
            }
        }
        //you're not in the temple yet.
        else{
            //walk to the temple, with a randomized path.
//            Walking.walkPath(Walking.randomizePath(Paths.pathTemple,1,1));
            mWalking.walkPath(Paths.pathTemple,Vars.get().gui_alch,Vars.get().itemName);
        }
    }

    @Override
    public boolean validate() {
        if (!Inventory.isFull() && Game.getGameState() == 30 && Calculations.gotItemsInventory(Vars.get().requiredItems)){
            if (!Player.getPosition().equals(Vars.get().castSpot)){
                if (!Player.isMoving()){
                    return true;
                }
            }
        }
        return false;
    }
}
