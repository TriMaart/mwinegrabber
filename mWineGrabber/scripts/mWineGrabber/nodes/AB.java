package scripts.mWineGrabber.nodes;

import org.tribot.api.General;
import org.tribot.api2007.Banking;
import org.tribot.api2007.Game;
import org.tribot.api2007.Skills;
import scripts.api.Node;
import scripts.api.AntiBan;
import scripts.mWineGrabber.user.Vars;

/**
 * Created by Jeroen on 9-8-2015.
 */
public class AB extends Node {

    @Override
    public void execute() {
        boolean ab = AntiBan.timedActions(Skills.SKILLS.MAGIC);
        if (ab) {
            Vars.get().STATE = "Antiban";
            General.println("ANTIBAN : " + ab);
        }


    }

    @Override
    public boolean validate() {
        return !Banking.isBankScreenOpen() && Game.getGameState() == 30;
    }
}
