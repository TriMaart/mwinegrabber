package scripts.mWineGrabber.nodes;

import org.tribot.api.Timing;
import org.tribot.api2007.Game;
import org.tribot.api2007.WorldHopper;
import scripts.api.Node;
import scripts.api.Login.WorldSwitcher;
import scripts.mWineGrabber.user.Vars;

/**
 * Created by Jeroen on 4-9-2015.
 */
public class TooFast extends Node {
    @Override
    public void execute() {
        Vars.get().STATE = "Handle too fast hopping.";

        Vars.get().time_exceed_login_limit = System.currentTimeMillis();
        Vars.get().script.setLoginBotState(false);
        WorldSwitcher.login(WorldHopper.getWorld());
        Vars.get().script.setLoginBotState(true);

    }

    @Override
    public boolean validate() {
        if (Game.getGameState() == 10 && Vars.get().gui_ingame && !Vars.get().script.isOnBreak() && Timing.timeFromMark(Vars.get().time_exceed_login_limit) > Vars.get().TIME_BETWEEN_TOO_FAST_HOPPING){
           return true;
        }
        return false;
    }
}
