package scripts.mWineGrabber.nodes;

import org.tribot.api.General;
import org.tribot.api.Timing;
import org.tribot.api.types.generic.Condition;
import org.tribot.api2007.*;
import org.tribot.api2007.types.RSInterfaceChild;
import org.tribot.api2007.types.RSItem;
import scripts.api.AntiBan;
import scripts.api.Calculations;
import scripts.api.Node;
import scripts.api.types.Spell;
import scripts.mWineGrabber.user.Paths;
import scripts.mWineGrabber.user.Vars;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Jeroen on 2-9-2015.
 */
public class Bank extends Node {


    @Override
    public void execute() {
        Vars.get().STATE = "Banking";
        //check if you're in the bank already
        AntiBan.activateRun();
        if (!Banking.isInBank()){
            Walking.walkPath(Walking.randomizePath(Paths.pathBank, 1, 1));
            Timing.waitCondition(new Condition() {
                @Override
                public boolean active() {
                    return !Player.isMoving();
                }
            }, General.random(6000,8000));
        }

        if (!Banking.isBankScreenOpen()){
            if (Banking.openBank()){
                depositAll();
                withrawRescources();
            }
        }
        else{
            depositAll();
            withrawRescources();
        }


    }

    @Override
    public boolean validate() {
        if (Inventory.isFull() || !Calculations.gotItemsInventory(Vars.get().requiredItems)){
            if (!Vars.get().temple.contains(Player.getPosition())){
                if (!Player.isMoving()) {
                    return true;

                }
            }
        }
        return false;
    }



    private void withrawRescources(){
        for (String name : Vars.get().requiredItems.keySet()){
            RSItem item = getItem(name);
            if (Vars.get().itemToAlch.keySet().contains(name)) {
                withdrawNoted(name);
                General.sleep(General.randomSD(500,800,700,50));
                continue;
            }

            if (item != null){
                if (item.getDefinition().isStackable()){
                    int stack = item.getStack();

                    if (stack < Vars.get().requiredItems.get(name)){
                        if (Calculations.supplyCheckBank(Vars.get().requiredItems)){
                            boolean succesfull = false;
                            while(!succesfull){
                                succesfull = Banking.withdraw(Vars.get().requiredItems.get(name) - stack,name);
                            }
                            General.sleep(General.randomSD(500,800,700,50));
                        }
                        else{
                            General.println("Not enough resources! Stopping now!");
                            Vars.get().stopping = true;
                            break;
                        }
                    }

                }
                else{
                    int count = Inventory.getCount(name);
                    if (count < Vars.get().requiredItems.get(name)){
                        if (Calculations.supplyCheckBank(Vars.get().requiredItems)){
                            boolean succesfull = false;
                            while(!succesfull){
                                succesfull = Banking.withdraw(Vars.get().requiredItems.get(name) - count,name);
                            }
                            General.sleep(General.randomSD(500,800,700,50));
                        }
                        else{
                            General.println("Not enough resources! Stopping now!");
                            Vars.get().stopping = true;
                            break;
                        }
                    }
                }
            }
            else{
                if (Calculations.supplyCheckBank(Vars.get().requiredItems)){
                    boolean succesfull = false;
                    while(!succesfull){
                        succesfull = Banking.withdraw(Vars.get().requiredItems.get(name),name);
                    }
                    General.sleep(General.randomSD(500,800,700,50));
                }
                else{
                    General.println("Not enough resources! Stopping now!");
                    Vars.get().stopping = true;
                    break;
                }
            }
        }
    }

    private void withdrawNoted(String name){
        RSItem item = getItem(name);

        if (item != null){
            int stack = item.getStack();
            if (stack < Vars.get().itemToAlch.get(name)){
                boolean succesfull = false;
                while(!succesfull){
                    if (setNoted(true)) {
                        succesfull = Banking.withdraw(Vars.get().itemToAlch.get(name) - stack, name);
                    }
                }

            }
        }
        else{
            boolean succesfull = false;
            while(!succesfull){
                if (setNoted(true)) {
                    succesfull = Banking.withdraw(Vars.get().itemToAlch.get(name), name);
                }
            }
        }
    }


    private void depositAll(){
        for (RSItem item : Inventory.filterDuplicates(Inventory.getAll())){
            if (!Vars.get().requiredItems.containsKey(item.getDefinition().getName())){
                Banking.depositItem(item,0);

            }
        }
    }

    private RSItem getItem(String name){
        RSItem[] item = Inventory.find(name);
        if (item.length > 0 && item[0]!=null){
            return item[0];
        }
        return null;
    }

    private final int 	BANK_MASTER_ID = 12,
            WITHDRAW_AS_ITEM_ID = 21,
            WITHDRAW_AS_NOTED_ID = 23;


    public boolean isNotedOn()
    {
        if(Banking.isBankScreenOpen())
        {
            RSInterfaceChild itemInterfaceChild = Interfaces.get(BANK_MASTER_ID, WITHDRAW_AS_ITEM_ID);
            if(itemInterfaceChild != null)
            {
                return itemInterfaceChild.getTextureID() == 812;
            }
        }
        return false;
    }

    public boolean setNoted(boolean noted)
    {
        if(Banking.isBankScreenOpen())
        {
            if(noted && !isNotedOn())
            {
                RSInterfaceChild notedInterfaceChild = Interfaces.get(BANK_MASTER_ID, WITHDRAW_AS_NOTED_ID);
                if(notedInterfaceChild != null && notedInterfaceChild.click("Note"))
                {
                    Timing.waitCondition(new Condition() {

                        @Override
                        public boolean active() {
                            General.sleep(10, 30);
                            return isNotedOn();
                        }
                    }, General.random(800, 1200));
                    return isNotedOn();
                }
            }
            else if(!noted && isNotedOn())
            {
                RSInterfaceChild itemInterfaceChild = Interfaces.get(BANK_MASTER_ID, WITHDRAW_AS_ITEM_ID);
                if(itemInterfaceChild != null && itemInterfaceChild.click("Item"))
                {
                    Timing.waitCondition(new Condition() {

                        @Override
                        public boolean active() {
                            General.sleep(10, 30);
                            return !isNotedOn();
                        }
                    }, General.random(800, 1200));
                    return !isNotedOn();
                }
            }
        }
        return false;
    }

}
