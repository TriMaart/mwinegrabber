package scripts.mWineGrabber.nodes;

import org.tribot.api.General;
import org.tribot.api2007.Inventory;
import org.tribot.api2007.WorldHopper;
import scripts.api.Login.WorldSwitcher;
import scripts.api.Node;
import scripts.api.Login.WorldSwitcherIngame;
import scripts.mWineGrabber.user.Vars;

/**
 * Created by Jeroen on 2-9-2015.
 */
public class WorldHop extends Node {
    @Override
    public void execute() {
        Vars.get().STATE = "World hopping";
        //disable login bot for fast hopping and relogging

        //check what method of world hopping the user selected
        if (Vars.get().gui_ingame) {
            //sometimes if you switch to much worlds, it will log out.
             if (WorldSwitcherIngame.switchWorld(getWorld())) {

                Vars.get().grabbedWine = false;
                Vars.get().lost = false;
            }
        }
        //user selected to use normal hop method (log-out -> log-in).
        else{
            Vars.get().script.setLoginBotState(false);
            if (WorldSwitcher.switchWorld(getWorld())){
                Vars.get().grabbedWine = false;
                Vars.get().lost = false;
            }
            Vars.get().script.setLoginBotState(true);
        }

    }

    private int getWorld(){
        int size = Vars.get().worlds.size() -1;
        int currentWorld = WorldHopper.getWorld();
        //check if random world hopping is enabled in custom world hopping.
        if (Vars.get().gui_rand_order){
            int rand = Vars.get().worlds.get(General.random(0,size));
            //check if the selected random world isn't the one you're in already.
            while (rand == currentWorld){
                rand = Vars.get().worlds.get(General.random(0,size));
            }
            //return a random world
            return rand;
        }
        //random hopping is disabled.
        else{
            //check if the current world is the last one in the list.
            if (Vars.get().worlds.indexOf(currentWorld) >= size){
//                General.println("Last world, starting from start again!");
                return Vars.get().worlds.get(0);
            }
            //current world is not the last one, hop to next one in the list.
            else{
                return Vars.get().worlds.get(Vars.get().worlds.indexOf(currentWorld)+1);
            }
        }
    }

    @Override
    public boolean validate() {
        if (Vars.get().gui_hop && !Inventory.isFull()){
            if (Vars.get().grabbedWine || Vars.get().lost){
                return true;
            }
        }
        return false;
    }
}
