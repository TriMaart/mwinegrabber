package scripts.mWineGrabber.nodes;

import org.tribot.api2007.Combat;
import org.tribot.api2007.Player;
import org.tribot.api2007.Walking;
import scripts.api.AntiBan;
import scripts.api.Node;
import scripts.mWineGrabber.user.Paths;
import scripts.mWineGrabber.user.Vars;

/**
 * Created by Jeroen on 7-9-2015.
 */
public class RunFromCombat extends Node {
    @Override
    public void execute() {
        Vars.get().STATE = "Running from combat!";
        AntiBan.activateRun();
        Walking.walkPath(Paths.escapePath);



    }

    @Override
    public boolean validate() {
        return Player.getRSPlayer().isInCombat() && !Player.isMoving();
    }
}
