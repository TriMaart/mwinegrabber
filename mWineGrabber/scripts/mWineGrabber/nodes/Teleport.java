package scripts.mWineGrabber.nodes;

import org.tribot.api.General;
import org.tribot.api.Timing;
import org.tribot.api.types.generic.Condition;
import org.tribot.api2007.Inventory;
import org.tribot.api2007.Magic;
import org.tribot.api2007.Player;
import scripts.api.Node;
import scripts.api.types.Spell;
import scripts.mWineGrabber.user.Vars;

/**
 * Created by Jeroen on 2-9-2015.
 */
public class Teleport extends Node {
    @Override
    public void execute() {
        Vars.get().STATE = "Teleporting to falador";

        if(Magic.selectSpell(Spell.FALADOR_TELEPORT.getName())){
            Timing.waitCondition(new Condition() {
                @Override
                public boolean active() {
                    return Vars.get().falador.contains(Player.getPosition());
                }
            }, General.random(2000,3000));
        }


    }

    @Override
    public boolean validate() {
        if (Inventory.isFull()){
            if (Vars.get().temple.contains(Player.getPosition())){
                return true;
            }
        }
        return false;
    }
}
