package scripts.mWineGrabber.nodes;

import org.tribot.api.General;
import org.tribot.api.Timing;
import org.tribot.api.input.Mouse;
import org.tribot.api.types.generic.Condition;
import org.tribot.api2007.*;
import org.tribot.api2007.types.RSGroundItem;
import org.tribot.api2007.types.RSItem;
import org.tribot.api2007.types.RSTile;
import scripts.api.AntiBan;
import scripts.api.Calculations;
import scripts.api.Node;
import scripts.api.types.Spell;
import scripts.mWineGrabber.user.Vars;

import java.awt.*;

/**
 * Created by Jeroen on 31-8-2015.
 */
public class GrabWine extends Node {
    private RSTile tile = new RSTile(2930, 3515, 0);

    @Override
    public void execute() {
        Vars.get().STATE = "Grabbing wine!";
        RSGroundItem wine = getWine();
        if (wine != null){

            //click and wait for the wine to be taken, or the message some one else got it.
            if(selectSpell(true,Spell.TELEKINECTIC_GRAB) && wine.click("Cast") && Timing.waitCondition(new Condition() {
                int cachedWines = Vars.get().winesGrabbed;

                public boolean active() {
                    return Vars.get().winesGrabbed > cachedWines || Vars.get().lost;
                }
            }, General.random(3000, 4000))){
                if (Vars.get().gui_hop && Timing.timeFromMark(Vars.get().time_exceed_login_limit) > Vars.get().TIME_BETWEEN_TOO_FAST_HOPPING && !Inventory.isFull()) {
                    Vars.get().grabbedWine = true;
                }
            }
        }
        //there is no wine to pick up yet.
        else {
            if (Vars.get().gui_alch) {
                highAlch(Vars.get().itemName);
                Timing.waitCondition(new Condition() {
                    @Override
                    public boolean active() {
                        return getWine()!=null || GameTab.getOpen().equals(GameTab.TABS.MAGIC);
                    }
                },General.random(4000,5000));

                //sleep for a random time after clicking item to alch
                General.sleep(General.randomSD(700, 1000, 850, 50));
            }
            //if not already hovering go hoverthe wine.
            else if(selectSpell(true,Spell.TELEKINECTIC_GRAB) && !Projection.getTileBoundsPoly(tile, 100).contains(Mouse.getPos())){
                Mouse.move(Calculations.centerOfATile(tile, 100));
            }
        }

        //reset the grabbedWine boolean if world hopping is disabled, or if you enabled alching
        if (!Vars.get().gui_hop || Vars.get().gui_alch){
            Vars.get().grabbedWine = false;
        }
    }

    @Override
    public boolean validate() {
        if (!Player.isMoving() && !Player.getRSPlayer().isInCombat() && Game.getGameState() != 10 && !Inventory.isFull() && Player.getPosition().equals(Vars.get().castSpot)) {
            //check if world hopping is enabled.
            if (Vars.get().gui_hop) {
                //check if you already grabbed a wine in this world.
                if (!Vars.get().grabbedWine && !Vars.get().lost) {
                    return true;
                }
            }
            //world hopping is disabled.
            else {
                return true;
            }
        }
        return false;
    }

    private RSGroundItem getWine(){
        RSGroundItem[] wine = GroundItems.findNearest("Wine of zamorak");
        if (wine.length > 0 && wine[0]!= null){
            return wine[0];
        }
        return null;
    }


    private boolean selectSpell(boolean select, Spell spell){
        if (!Calculations.gotItemsInventory(spell.getRunes(1))){
            General.println("Not enough runes! Stopping now!");
            Vars.get().stopping = true;
            return false;
        }

        if (select) {
            if (Magic.isSpellSelected()) {
                //check if the selected magic spell is telegrab.
                if (Magic.getSelectedSpellName().equals(spell.getName())) {
                    return true;
                }
                //wrong spell is selected
                else {
                    //deselect wrong spell.
                    Mouse.click(1);
                    //select the telegrab.
                    if (Magic.selectSpell(spell.getName())) {
                        return true;
                    }
                }
            }
            //there is no spell selected yet
            else {
                if (Magic.selectSpell(spell.getName())) {
                    return true;
                }
            }
            //failed to select a spell
            return false;
        }
        //deselect the spell (select is false)
        else{
            if (Magic.isSpellSelected()){
                Mouse.click(1);
            }
            return Magic.isSpellSelected();

        }

    }

    private void highAlch(String itemName){
        if (Calculations.gotItemsInventory(Spell.HIGH_LEVEL_ALCHEMY.getRunes(1))) {
            RSItem[] alchItem = Inventory.find(itemName);
            if (alchItem.length > 0 && alchItem[0] != null) {
                if (alchItem[0].getIndex() != 15) {
                    moveItem(itemName, 15);
                    alchItem = Inventory.find(itemName);
                    //null check still necessary?

                }
                if (selectSpell(true, Spell.HIGH_LEVEL_ALCHEMY)) {

                    alchItem[0].click("Cast");
                    AntiBan.waitItemInteractionDelay();

                }
            }
            else{
                General.println("Not enough " + itemName + " to alch.");
                Vars.get().stopping = true;
            }

        }
        else{
            General.println("Not enough runes!");
            Vars.get().stopping = true;
        }
    }

    private static void moveItem(String name, int inventorySlot){
        RSItem placeholder = new RSItem(inventorySlot,-1,1, RSItem.TYPE.INVENTORY);
        RSItem[] item = Inventory.find(name);
        GameTab.open(GameTab.TABS.INVENTORY);

        if (item.length > 0 && item[0] != null){
            Mouse.moveBox(item[0].getArea());
            Mouse.drag(Mouse.getPos(), Calculations.randomPointRectangle(placeholder.getArea()),1);
        }
    }


}
