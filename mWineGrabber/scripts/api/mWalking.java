package scripts.api;

import org.tribot.api.General;
import org.tribot.api.input.Mouse;
import org.tribot.api2007.*;
import org.tribot.api2007.ext.Doors;
import org.tribot.api2007.types.RSItem;
import org.tribot.api2007.types.RSTile;
import org.tribot.api2007.util.DPathNavigator;
import scripts.api.types.Spell;

import java.util.Arrays;
import java.util.Collections;

/**
 * Created by Jeroen on 8-9-2015.
 */
public class mWalking {
    private final static DPathNavigator navigator = new DPathNavigator();
    private static boolean highAlch = false;

    public static void walkPath(RSTile[] path, boolean alch, String name){
        boolean finished = false;
        highAlch = alch;
        Collections.reverse(Arrays.asList(path));
        while(!finished) {
            highAlch(name);
            for (RSTile t : path) {
                if (Projection.isInMinimap(Projection.tileToMinimap(t))) {
                    if (Game.getDestination() == null || t.distanceTo(Game.getDestination()) > 3) {
                        RSTile[] d_path = navigator.findPath(t);
                        RSTile doorTile = containsClosedDoor(d_path);
                        if (doorTile != null) {
                            Walking.walkTo(doorTile);
                            selectSpell(false);
                            Doors.handleDoorAt(doorTile, true);
                        } else {
                            Walking.walkTo(t);
                        }
//                    General.println("Click");
                    }

                    if (path[0].equals(Game.getDestination())){
                        finished = true;
                        General.println("Last tile clicked");
                    }

                    break;
                }
            }
            General.sleep(General.randomSD(700,1000,850,50));
        }
    }

    private static RSTile containsClosedDoor(RSTile[] path){
        for (RSTile t : path){
            if (Doors.isDoorAt(t,false)){
                return t;
            }
        }
        return null;
    }

    private static boolean selectSpell(boolean select) {
        if (select) {
            if (Magic.isSpellSelected()) {
                //check if the selected magic spell is telegrab.
                if (Magic.getSelectedSpellName().equals(Spell.HIGH_LEVEL_ALCHEMY.getName())) {
                    return true;
                }
                //wrong spell is selected
                else {
                    //deselect wrong spell.
                    Mouse.click(1);
                    //select the telegrab.
                    if (Magic.selectSpell(Spell.HIGH_LEVEL_ALCHEMY.getName())) {
                        return true;
                    }
                }
            }
            //there is no spell selected yet
            else {
                if (Magic.selectSpell(Spell.HIGH_LEVEL_ALCHEMY.getName())) {
                    return true;
                }
            }
            //failed to select a spell
            return false;

        }
        else{
            if (Magic.isSpellSelected()){
                Mouse.click(1);
            }
            return Magic.isSpellSelected();

        }
    }

    private static void highAlch(String name){
        if (Calculations.gotItemsInventory(Spell.HIGH_LEVEL_ALCHEMY.getRunes(1))) {
            RSItem[] alchItem = Inventory.find(name);
            if (alchItem[0].getIndex() != 15){
                moveItem(name,15);
            }
            if (selectSpell(true)) {
                if (alchItem.length > 0 && alchItem[0] != null) {
                    alchItem[0].click("Cast");
                    AntiBan.waitItemInteractionDelay();
                }
                else{
                    highAlch = false;
                }
            }
        }
        else{
            highAlch = false;
        }
    }

    private static void moveItem(String name, int inventorySlot){
        RSItem placeholder = new RSItem(inventorySlot,-1,1, RSItem.TYPE.INVENTORY);
        RSItem[] item = Inventory.find(name);


        if (item.length > 0 && item[0] != null){
            Mouse.moveBox(item[0].getArea());
            Mouse.drag(Mouse.getPos(), Calculations.randomPointRectangle(placeholder.getArea()),1);
        }
    }
}
