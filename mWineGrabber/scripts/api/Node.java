package scripts.api;

/**
 * Created by Jeroen on 14-7-2015.
 */
public abstract class Node {

    protected ACamera aCamera;
    public boolean end = false;
    public Node(ACamera aCamera)
    {
        this.aCamera = aCamera;
    }

    public Node(){};



    public abstract void execute();
    public abstract boolean validate();
}

