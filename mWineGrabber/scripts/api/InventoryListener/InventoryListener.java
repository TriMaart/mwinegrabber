package scripts.api.InventoryListener;

/**
 * Created by Jeroen on 7-8-2015.
 */
public interface InventoryListener {
    public void inventoryItemGained(int id, int count);
    public void inventoryItemLost(int id, int count);
}
