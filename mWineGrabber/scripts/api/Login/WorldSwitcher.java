package scripts.api.Login;

import org.tribot.api.General;
import org.tribot.api.Timing;
import org.tribot.api.input.Keyboard;
import org.tribot.api.input.Mouse;
import org.tribot.api.types.generic.Condition;
import org.tribot.api2007.*;
import org.tribot.api2007.types.RSInterface;

import java.awt.*;

/**
 * Created by Jeroen on 4-9-2015.
 */
public class WorldSwitcher {
    private static Thread cachedLoginThread = LoginThread.createLoginThread();

    public static final Rectangle LOGIN = new Rectangle(235,307,132,27);
    private static final Rectangle CANCEL = new Rectangle(399,307,132,27);
    private static final Rectangle EXISTING_USER = new Rectangle(397,277,132,27);

    private static final int[] WORLD_COLUMN_1 = {1,2,3,4,5,6,8,9,10,11,12,13,14,16,17,18,19,20},
            WORLD_COLUMN_2 = {21,22,25,26,27,28,29,30,33,34,35,36,37,38,41,42,43,44},
            WORLD_COLUMN_3 = {45,46,49,50,51,52,53,54,57,58,59,60,61,62,65,66,67,68},
            WORLD_COLUMN_4 = {69,70,73,74,75,76,77,78,81,82,83,84,85,86,93,94};
    private static final int[][]   WORLD_LIST = {WORLD_COLUMN_1, WORLD_COLUMN_2, WORLD_COLUMN_3, WORLD_COLUMN_4};

    private static final int topX = 220;
    private static final int topY =  56;
    private static final int X_MULTIPLIER = 100;
    private static final int Y_MULTIPLIER =  24;

    private static final int mouseCache = Mouse.getSpeed();
    private static final double keyboardCache = Keyboard.getSpeed();



    public static boolean switchWorld(int world){
        if (Game.getGameState() != 10) {
            if (WorldHopper.getWorld() == world) {
                return true;
            }
            if (!GameTab.open(GameTab.TABS.LOGOUT) || !Login.logout()) {
                return false;
            }
        }

        Mouse.setSpeed(1000);
        Keyboard.setSpeed(0.1);
        return openWorldScreen() && selectWorld(world) && login(world);

    }

    private static boolean isLoggedOut(){
        return (Game.getGameState() == 10);
    }

    private static boolean openWorldScreen(){
        if(isLoggedOut()){

            return Timing.waitCondition(new Condition() {
                @Override
                public boolean active() {
                    Mouse.clickBox(10, 471, 88, 488, 1);
                    General.sleep(500, 700);
                    return isWorldScreenOpen();
                }
            }, General.random(6000, 2000));
        }
        return false;
    }

    private static boolean isWorldScreenOpen(){
        Color wScreen = Screen.getColorAt(100, 100);
        return (wScreen.getRed() == 0 && wScreen.getGreen() == 0 && wScreen.getBlue() == 0);
    }

    private static boolean selectWorld(int world){
        int wNum = 0,
                xPos = 0,
                yPos = 0;

        for(int x = 0; x < WORLD_LIST.length; x++){
            for(int y = 0; y < WORLD_LIST[x].length; y++){
                wNum = WORLD_LIST[x][y];
                if(wNum == world){
                    xPos = x * X_MULTIPLIER + topX;
                    yPos = y * Y_MULTIPLIER + topY;



                    Mouse.click(xPos, yPos, 1);
                    return Timing.waitCondition(new Condition() {
                        @Override
                        public boolean active() {
                            return !isWorldScreenOpen();
                        }
                    },General.random(1000,2000));
                }
            }
        }
        General.println("World not found!");
        return false;
    }

    private static boolean existingUserUp(){
        Color existing = Screen.getColorAt(442,253);
        return (existing.getRed() == 255 && existing.getGreen() == 255 && existing.getBlue() == 0);
    }

    public static boolean login(int world){
        cachedLoginThread.interrupt();
        Mouse.setSpeed(1000);
        Keyboard.setSpeed(0.1);

        //clicks the existing user button
        Mouse.clickBox(407,284,522,300,1);



        Timing.waitCondition(new Condition() {
            @Override
            public boolean active() {
                return Game.getGameState() == 20;
            }
        },General.random(7000,9000));

        //if the first login failed, handle the error.
        while (!Login.getLoginState().equals(Login.STATE.WELCOMESCREEN)){
            General.sleep(500);
            final String loginMessage = Login.getLoginResponse();
            switch (loginMessage){
                case "Enter your username/email & password.":
                    Timing.waitCondition(new Condition() {
                        @Override
                        public boolean active() {
                            return !Login.getLoginResponse().equals(loginMessage) || Game.getGameState() != 10;
                        }
                    },General.random(5000,6000));
//                    Login.login();
                    break;
                case "Connecting to server...":
                    Timing.waitCondition(new Condition() {
                        @Override
                        public boolean active() {
                            return !Login.getLoginResponse().equals(loginMessage) || Game.getGameState() == 30;
                        }
                    },General.random(5000,6000));
                    break;
                case "Error connecting to server.":
                    Mouse.clickBox(LOGIN,1);
                    break;
                case "Invalid username/email or password.":
                    Mouse.clickBox(CANCEL,1);
                    Mouse.clickBox(EXISTING_USER,1);
                    break;
                case "Your account is already logged in. Try again in 60 secs...":
                    General.sleep(3000,5000);
                    Mouse.clickBox(LOGIN,1);
                    break;
                case "You need a skill total of 1250 to play on this world.":
                    openWorldScreen();
                    selectWorld(world);
//                    Login.login();
                    break;
                case "Too many login attempts. Please wait a few minutes before trying again.":
//                    General.println("Too many!");
                    General.sleep(500,1000);
                    Mouse.clickBox(LOGIN,1);
                    break;
                case "Your account has been disabled. Please check your message-centre for details.":
                    General.sleep(20000000);
                    break;
                case "Account locked as we suspect it has been stolen. Press 'recover a locked account' on front page.":
                    General.sleep(20000000);
                    break;

            }
        }


//        if (Timing.waitCondition(new Condition() {
//            @Override
//            public boolean active() {
//                return Interfaces.get(378,17) != null;
//            }
//        },General.random(1000,1500))){
//            //should null check?
//            Interfaces.get(378,17).click();
//            General.println("Clicked");
//        }
        while(Login.getLoginState().equals(Login.STATE.WELCOMESCREEN)) {
            Mouse.clickBox(277, 298, 492, 377,1);
        }

        Mouse.setSpeed(mouseCache);
        Keyboard.setSpeed(keyboardCache);

        return true;
    }
}
