package scripts.api.Login;

/**
 * Created by Jeroen on 31-8-2015.
 */

import org.tribot.api.Clicking;
import org.tribot.api.General;
import org.tribot.api.Timing;
import org.tribot.api.input.Mouse;
import org.tribot.api.types.generic.Condition;
import org.tribot.api2007.*;
import org.tribot.api2007.types.RSInterface;
import org.tribot.api2007.types.RSInterfaceChild;
import org.tribot.api2007.types.RSInterfaceComponent;

import java.awt.*;
import java.util.HashMap;

public class WorldSwitcherIngame {

    private static final HashMap<Integer, Integer> CACHE = new HashMap<>();

    public static boolean switchWorld(int world) {
        if (WorldHopper.getWorld() == world) {
            return true;
        }
        if (!GameTab.open(GameTab.TABS.LOGOUT) || !openWorldSwitchInterface() || !moveMouseInsideWorldSwitchInterface() || Game.getGameState() == 10) {
            return false;
        }

        final RSInterfaceChild loading = Interfaces.get(69, 2);
        if (loading != null){
            Timing.waitCondition(new Condition() {
                @Override
                public boolean active() {
                    return !loading.getText().equals("Loading...");
                }
            },General.random(3000,4000));
        }

        RSInterfaceComponent desiredWorld = getWorldComponent(world);
        return desiredWorld != null && scrollToWorldInterface(desiredWorld) && clickWorldSwitchAndClickYes(desiredWorld) && waitUntilWorld(world);
    }

    private static boolean openWorldSwitchInterface() {
        if (Interfaces.isInterfaceValid(182)) {
            RSInterfaceChild child = Interfaces.get(182, 5);
            if (child == null || !child.click() || !Timing.waitCondition(new Condition() {
                @Override
                public boolean active() {
                    General.sleep(50, 100);
                    return Interfaces.isInterfaceValid(69);
                }
            }, General.random(1200, 2200))) {
                return false;
            }
        }
        return true;
    }

    private static boolean moveMouseInsideWorldSwitchInterface() {
        RSInterfaceChild worldSelectBox = Interfaces.get(69, 4);
        if (worldSelectBox != null) {
            Mouse.moveBox(worldSelectBox.getAbsoluteBounds());

            return true;
        }
        return false;
    }

    public static RSInterfaceComponent getWorldComponent(int world) {
        RSInterfaceChild worldList = Interfaces.get(69, 7);

        if (worldList.getChildren() != null) {
            if (CACHE.containsKey(world)) {
                if (worldList.getChildren().length > CACHE.get(world)) {
                    return worldList.getChildren()[CACHE.get(world)];
                }
                else {
                    //Worlds haven't loaded yet.
//                    General.println("WTF no worlds found!");
                }
            }

            for (RSInterfaceComponent i : worldList.getChildren()) {
                if (i.getText().equals(String.valueOf(world))) {
                    CACHE.put(world, i.getIndex() - 2);
                    return worldList.getChildren()[i.getIndex() - 2];
                }
            }
        }

        return null;
    }


    private static boolean scrollToWorldInterface(RSInterfaceComponent desiredWorld) {
        final long timeout = System.currentTimeMillis() + 7000;
        Rectangle worldClickBox;
        while ((worldClickBox = desiredWorld.getAbsoluteBounds()) != null && (worldClickBox.y < 229 || worldClickBox.y > 417)) {
            Mouse.scroll(worldClickBox.y < 229);
            General.sleep(50, 100);
            if (System.currentTimeMillis() > timeout) {
                return false;
            }
        }
        return worldClickBox != null && worldClickBox.y > 229 && worldClickBox.y < 417;
    }

    private static boolean clickWorldSwitchAndClickYes(RSInterfaceComponent desiredWorld) {
        if (Clicking.click(desiredWorld)) {
            //checks if there is a chat.
            if (Timing.waitCondition(new Condition() {
                public boolean active() {
                    General.sleep(100, 300);
                    return NPCChat.getOptions() != null;
                }
            }, General.random(2000, 3000))) {
                //the is a chat.
                return NPCChat.selectOption("Yes.", true);
            }
            //the is no chat, so we assume the user disabled this.
            else {
                return true;
            }

        }
        //couldn't click the desired world
        else {
            return false;
        }
    }

    private static boolean waitUntilWorld(final int world) {
        return Timing.waitCondition(new Condition() {
            @Override
            public boolean active() {
                General.sleep(100, 300);
                return WorldHopper.getWorld() == world;
            }
        }, General.random(14000, 16000));
    }
}