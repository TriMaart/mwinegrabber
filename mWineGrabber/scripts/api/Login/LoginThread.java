package scripts.api.Login;

import org.tribot.api.General;
import org.tribot.api.input.Mouse;
import org.tribot.api2007.Game;
import org.tribot.api2007.Login;
import org.tribot.api2007.Screen;

import java.awt.*;

/**
 * Created by Jeroen on 4-9-2015.
 */
public class LoginThread extends Thread {

    private LoginThread(){
        setDaemon(true);
    }

    public static Thread createLoginThread(){
        LoginThread loginThread = new LoginThread();
        Thread thread = new Thread(loginThread);
        thread.start();
        return thread;
    }

    @Override
    public void run() {
        General.println("Thread Started");
        while (true) {
            while (loginUserUp() && Game.getGameState() == 10) {
//                General.println("Logging in!");
                Login.login();
//                General.println("Done logging in!");
            }

            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                General.println("Interrupted");
                e.printStackTrace();
            }


        }
    }


    private boolean loginUserUp(){
        Color login = Screen.getColorAt(463,324);
        return (login.getRed() == 255 && login.getGreen() == 255 && login.getBlue() == 255);
    }
}
