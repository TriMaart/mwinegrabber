package scripts.api;

import org.tribot.api.General;
import org.tribot.api.interfaces.Positionable;
import org.tribot.api2007.Camera;
import org.tribot.api2007.Camera.ROTATION_METHOD;
import org.tribot.api2007.Player;
import org.tribot.script.Script;
/**
 * 
 * @author Final Calibur & WastedBro (no specific order)
 *
 */
public class ACamera 
{
	//Local constants
	private final int ROTATION_THRESHOLD 	= 30;	//We will not change the rotation when turning to a tile if the current rotation is +- this amount from the optimal value
	private final int ANGLE_THRESHOLD		= 15; 	//Same as above, but for angle
	private final int TURN_TO_TILE_DELAY	= 1500; //The minimum wait time between turnToTile calls. This helps avoid scripts that may spam call the method, which would look very bot-like.
	
    //Private instance fields
    private long					lastTurnToTile;	//The last time we used the turnToTile method (to avoid scripts spamming it and looking like a bot)
    private RotationThread          rotationThread;	//The thread that will handle camera rotation
    private AngleThread             angleThread;	//The thread that will handle camera angle 
    private Script					script;			//The script we are handling the camera for. This is so we can know when to shut off the threads.

    
    /**
     * Default constructor.
     */
    public ACamera(Script s)
    {
        instantiateVars(s);
    }

    /**
     *	This method instantiates all of our variables for us.
     *	I decided to make this method because it makes instantiated the variables in the constructors less redundant.
     *
     * 	@param s script we are controlling camera for
     * 	@param sleepTime sleep time between cycles
     */
    private void instantiateVars(Script s)
    {
    	this.script = s;
        this.rotationThread = new RotationThread();
        this.rotationThread.setName("ACamera Rotation Thread");
        this.angleThread = new AngleThread();
        this.angleThread.setName("ACamera Angle Thread");
        this.lastTurnToTile = System.currentTimeMillis();
        this.rotationThread.start();
        this.angleThread.start();
        Camera.setRotationMethod(ROTATION_METHOD.ONLY_KEYS);
    }
    
    /**
     * Sets the camera angle
     * @param specified angle to set camera to
     */
    public void setAngle(int angle)
    {
    	synchronized(this.angleThread)
    	{
    		this.angleThread.angle = angle;
    		this.angleThread.notify();
    	}
    }
    
    /**
     * Sets the camera rotation
     * @param rotation specified rotation to set camera to
     */
    public void setRotation(int rotation)
    {
    	synchronized(this.rotationThread)
    	{
	        this.rotationThread.rotation = rotation;
	        this.rotationThread.notify();
    	}
    }
    
    /**
     * 	Turns the camera to a specific tile.
     * 
     * 	@param tile to turn to
     */
    public void turnToTile(Positionable tile)
    {
    	if(System.currentTimeMillis() - lastTurnToTile > TURN_TO_TILE_DELAY)
    	{   
	        int optimalAngle = adjustAngleToTile(tile);
	        int optimalRotation = Camera.getTileAngle(tile);
	        
	        if(Math.abs(optimalAngle - Camera.getCameraAngle()) > ANGLE_THRESHOLD)
	        {
	        	setAngle(optimalAngle + General.random(-12, 12));
	        }
	        
	        if(Math.abs(optimalRotation - Camera.getCameraRotation()) > ROTATION_THRESHOLD)
	        {
	        	setRotation(optimalRotation + General.random(-30, 30));
	        }
	        
	        if(!tile.getPosition().isOnScreen())
	        {
	        	setAngle(optimalAngle + General.random(-3, 3));
	        	setRotation(optimalRotation + General.random(-3, 3));
	        }
	        
	        lastTurnToTile = System.currentTimeMillis();
    	}
    }
    
    /**
     * Adjusts the angle of the camera so that it is optimal for the tile
     * @param tile Tile to adjust angle for
     * @return the optimal angle to view this tile at
     */
    private int adjustAngleToTile(Positionable tile)
    {
    	//Distance from player to object - Used in calculating the optimal angle.
    	//Objects that are farther away require the camera to be turned to a lower angle to increase viewing distance.
    	int distance = Player.getPosition().distanceTo(tile);
    	
    	//The angle is calculated by taking the max height (100, optimal for very close objects),
    	//and subtracting an arbitrary number (I chose 6 degrees) for every tile that it is away.
        // 6 normally
    	int angle = 100 - (distance * 2);
    	
    	return angle;
    }
    
    private class RotationThread extends Thread
    {	
        protected int rotation = Camera.getCameraRotation();
        
        @Override
        public synchronized void run() 
        {
        	try
        	{  
        		while(script != null)
        		{       		
		            Camera.setCameraRotation(rotation);
		               
		            getRotationThread().wait();
        		}
        	}
        	catch(Exception e)
        	{
        		General.println("Error initiating wait on angle thread.");
        	}
        }
        
    }
       
    private class AngleThread extends Thread
    {
        protected int angle = Camera.getCameraAngle();
        
        @Override
        public synchronized void run() 
        {
        	try
        	{   
        		while(script != null)
        		{      			       		
		            Camera.setCameraAngle(angle);
		               
		            getAngleThread().wait();      
        		}
        	}
        	catch(Exception e)
        	{
        		General.println("Error initiating wait on angle thread.");
        	}
        }
        
    }

    public RotationThread getRotationThread() 
    {
        return rotationThread;
    }

    public AngleThread getAngleThread() 
    {
        return angleThread;
    }
  
    public Script getScript() {
		return script;
	}
    
}