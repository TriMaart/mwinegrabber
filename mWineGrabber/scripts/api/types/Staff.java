package scripts.api.types;

import org.tribot.api2007.Equipment;
import org.tribot.api2007.types.RSItem;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Jeroen on 8-9-2015.
 */
public enum Staff {
    STAFF_OF_AIR("Staff of air",new ArrayList<String>(){
        {
            add("Air rune");
        }
    }),
    STAFF_OF_WATER("Staff of water",new ArrayList<String>(){
        {
            add("Water rune");
        }
    }),
    STAFF_OF_EARTH("Staff of earth",new ArrayList<String>(){
        {
            add("Earth rune");
        }
    }),
    STAFF_OF_FIRE("Staff of fire",new ArrayList<String>(){
        {
            add("Fire rune");
        }
    });


    private String name;

    private ArrayList<String> runes;

    Staff(String name, ArrayList<String> runes) {
        this.name = name;
        this.runes = runes;
    }

    public String getName() {
        return name;
    }

    public ArrayList<String> getRunes() {
        return runes;
    }
}
