package scripts.api.types;

import org.tribot.api.General;
import org.tribot.api2007.Equipment;
import org.tribot.api2007.Inventory;
import org.tribot.api2007.types.RSItem;

import java.util.HashMap;

/**
 * Created by Jeroen on 31-8-2015.
 */
public enum Spell {

    TELEKINECTIC_GRAB("Telekinetic Grab",33,new HashMap<String,Integer>(){
        {
            put("Air rune",1);
            put("Law rune",1);
        }
    }),
    FALADOR_TELEPORT("Falador Teleport",37,new HashMap<String,Integer>(){
        {
            put("Water rune", 1);
            put("Air rune", 3);
            put("Law rune", 1);
        }
    }),
    HIGH_LEVEL_ALCHEMY("High Level Alchemy",55,new HashMap<String,Integer>(){
        {
            put("Nature rune", 1);
            put("Fire rune", 5);
        }
    });


    private String name;
    private int level;
    private HashMap<String,Integer> runes;

    Spell(String name, int level, HashMap<String, Integer> runes) {
        this.name = name;
        this.level = level;
        this.runes = runes;
    }

    public String getName() {
        return name;
    }

    public int getLevel() {
        return level;
    }

    public HashMap<String, Integer> getRunes(int casts) {
        HashMap<String,Integer> temp = new HashMap<>();
        Staff staff = getStaff();

        for (String key : runes.keySet()){
            int value = runes.get(key);
            value *= casts;
            temp.put(key, value);
        }

        if (staff != null){
            for (String s : staff.getRunes()){
                temp.remove(s);
            }
            return temp;
        }
        return temp;
    }

    private Staff getStaff(){
        RSItem[] weapon = Equipment.find(Equipment.SLOTS.WEAPON);
        if (weapon.length > 0 && weapon[0] != null && weapon[0].getDefinition().getName().contains("Staff")){
            switch (weapon[0].getDefinition().getName()) {
                case "Staff of air":
                    return Staff.STAFF_OF_AIR;
                case "Staff of earth":
                    return Staff.STAFF_OF_EARTH;
                case "Staff of water":
                    return Staff.STAFF_OF_WATER;
                case "Staff of fire":
                    return Staff.STAFF_OF_FIRE;
            }
        }
        return null;
    }
}
