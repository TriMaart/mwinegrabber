package scripts.api.types;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;

/**
 * Created by Jeroen on 1-9-2015.
 */
public class Worlds {
    //Excluded 86 and 85, as its a bot watch world.
    public static final ArrayList<Integer> ALL_WORLDS =  new ArrayList<Integer>(Arrays.asList(1, 2, 3, 4, 5, 6, 8, 9, 10, 11, 12, 13, 14, 16, 17, 18, 19, 20, 21, 22, 25, 26, 27, 28, 29, 30,
            33, 34, 35, 36, 37, 38, 41, 42, 43, 44, 45, 46, 49, 50, 51, 52, 53, 54, 57, 58, 59, 60, 61, 62, 65, 66, 67, 68, 69, 70, 73, 74, 75,
            76, 77, 78, 81, 82, 83, 84, 93, 94));

    //Excludedd 85 as its a bot watch world
    public static final ArrayList<Integer> F2P_WORLDS = new ArrayList<Integer>(Arrays.asList(1, 8, 16, 26, 35, 81, 82, 83 ,84 ,93 ,94));

    public static final ArrayList<Integer> PVP_WORLDS = new ArrayList<Integer>(Arrays.asList(25,37));

    public static final ArrayList<Integer> TOTAL_WORLDS = new ArrayList<>(Arrays.asList(53,61,66,73));

    public static final ArrayList<Integer> P2P_WORLDS = getArray();

    private static ArrayList<Integer> getArray(){
        ArrayList<Integer> array = new ArrayList<Integer>();
        array.addAll(ALL_WORLDS);
        array.removeAll(F2P_WORLDS);
        array.removeAll(PVP_WORLDS);
        array.removeAll(TOTAL_WORLDS);
        return array;
    }

    public static ArrayList<Integer> getCustomArray(boolean include_f2p,boolean include_p2p,boolean include_total){
        ArrayList<Integer> array = new ArrayList<Integer>();
        array.addAll(ALL_WORLDS);
        array.removeAll(PVP_WORLDS);
        if (!include_f2p){
            array.removeAll(F2P_WORLDS);
        }
        if (!include_p2p){
            array.removeAll(P2P_WORLDS);
        }
        if (!include_total){
            array.removeAll(TOTAL_WORLDS);
        }
        return array;
    }
}
