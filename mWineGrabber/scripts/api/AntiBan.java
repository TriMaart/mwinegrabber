package scripts.api;
import org.tribot.api.Clicking;
import org.tribot.api.DynamicClicking;
import org.tribot.api.General;
import org.tribot.api.Timing;
import org.tribot.api.input.Mouse;
import org.tribot.api.interfaces.Positionable;
import org.tribot.api.types.generic.Condition;
import org.tribot.api.types.generic.Filter;
import org.tribot.api.util.ABCUtil;
import org.tribot.api.util.Sorting;
import org.tribot.api2007.*;
import org.tribot.api2007.Skills.SKILLS;
import org.tribot.api2007.types.RSCharacter;
import org.tribot.api2007.types.RSItem;
import org.tribot.api2007.types.RSNPC;
import org.tribot.api2007.types.RSNPCDefinition;
import org.tribot.api2007.types.RSObject;
import org.tribot.api2007.types.RSObjectDefinition;

import java.util.Arrays;


/**
 * The AntiBan class provides an easy way to implement Anti-ban Compliance into any script.
 * <p>
 * When any method that is not a delay/wait is called, the tracker is checked. If the tracker is ready, the action will be performed
 * and the tracker is reset unless specified otherwise.
 * If the tracker is not ready, nothing will happen and the method will return.
 * <p>
 * @author Starfox
 * @version 1.3 (4/16/2015 1:35 PM UTC -5:00)
 */
public final class AntiBan {

    /**
     * The object that stores the seeds.
     */
    private static final ABCUtil abc;

    /**
     * The amount of resources you have won.
     */
    private static int resources_won;

    /**
     * The amount of resources you have lost.
     */
    private static int resources_lost;

    /**
     * Static initialization block.
     * <p>
     * By default, the use of general anti-ban compliance is set to be true.
     */
    static {
        abc = new ABCUtil();
        resources_won = 0;
        resources_lost = 0;
        General.useAntiBanCompliance(true);
    }

    /**
     * Prevent instantiation of this class.
     */
    private AntiBan() {

    }

    /**
     * Gets the ABCUtil object.
     * <p>
     * @[member="Return"] The ABCUtil object.
     */
    public static final ABCUtil getABCUtil() {
        return abc;
    }

    /**
     * Gets the amount of resources won.
     * <p>
     * @[member="Return"] The amount of resources won.
     */
    public static final int getResourcesWon() {
        return resources_won;
    }

    /**
     * Gets the amount of resources lost.
     * <p>
     * @[member="Return"] The amount of recourses lost.
     */
    public static final int getResourcesLost() {
        return resources_lost;
    }

    /**
     * Sets the amount of resources won to the specified amount.
     * <p>
     * @param amount The amount to set.
     */
    public static final void setResourcesWon(int amount) {
        resources_won = amount;
    }

    /**
     * Sets the amount of resources lost to the specified amount.
     * <p>
     * @param amount The amount to set.
     */
    public static final void setResourcesLost(int amount) {
        resources_lost = amount;
    }

    /**
     * Increments the amount of resources won by 1.
     */
    public static final void incrementResourcesWon() {
        resources_won++;
    }

    /**
     * Increments the amount of resources lost by 1.
     */
    public static final void incrementResourcesLost() {
        resources_lost++;
    }

    /**
     * Checks to see whether or not the specified bool tracker is ready.
     * <p>
     * @param tracker The tracker to check.
     * @[member="Return"] True if it is ready, false otherwise.
     */
    public static final boolean isBoolTrackerReady(ABCUtil.BOOL_TRACKER tracker) {
        return tracker.next();
    }

    /**
     * Checks to see whether or not the specified switch tracker is ready.
     * <p>
     * @param tracker     The tracker to check.
     * @param playerCount The player count.
     * @[member="Return"] True if it is ready, false otherwise.
     */
    public static final boolean isSwitchTrackerReady(ABCUtil.SWITCH_TRACKER tracker, int playerCount) {
        return tracker.next(playerCount);
    }

    /**
     * Gets the time (in milliseconds) until the next action should be performed for the specified delay tracker.
     * <p>
     * @param tracker The tracker.
     * @[member="Return"] The time (in milliseconds) until the next action should be performed.
     */
    public static final long getTimeUntilNext(ABCUtil.DELAY_TRACKER tracker) {
        return tracker.next() - System.currentTimeMillis();
    }

    /**
     * Gets the time (in milliseconds) until the next action should be performed for the specified int tracker.
     * <p>
     * @param tracker The tracker.
     * @[member="Return"] The time (in milliseconds) until the next action should be performed.
     */
    public static final long getTimeUntilNext(ABCUtil.INT_TRACKER tracker) {
        return tracker.next() - System.currentTimeMillis();
    }

    /**
     * Gets the time (in milliseconds) until the next action should be performed for the specified time tracker.
     * <p>
     * @param tracker The tracker.
     * @[member="Return"] The time (in milliseconds) until the next action should be performed.
     */
    public static final long getTimeUntilNext(ABCUtil.TIME_TRACKER tracker) {
        return tracker.next() - System.currentTimeMillis();
    }

    /**
     * Gets the next run energy at which run should be enabled at if it is not already enabled.
     * <p>
     * @[member="Return"] The next run energy.
     */
    public static final int getNextRunEnergy() {
        return getABCUtil().INT_TRACKER.NEXT_RUN_AT.next();
    }

    /**
     * Gets the next eat percent at which the player should eat at.
     * <p>
     * @[member="Return"] The next eat percent.
     */
    public static final int getNextEatPercent() {
        return getABCUtil().INT_TRACKER.NEXT_EAT_AT.next();
    }

    /**
     * Activates run.
     * <p>
     * No action is taken if run is already enabled or the current run energy is less than the value returned by {@[member="Link"] #getNextRunEnergy()}.
     * <p>
     * @[member="Return"] True if run was enabled, false otherwise.
     * @[member="see"] #getNextRunEnergy()
     */
    public static final boolean activateRun() {
        if (Game.getRunEnergy() >= getNextRunEnergy() && !Game.isRunOn()) {
            if (Options.setRunOn(true)) {
                getABCUtil().INT_TRACKER.NEXT_RUN_AT.reset();
                return true;
            }
        }
        return false;
    }

    /**
     * Randomly moves the camera. Happens only if the time tracker for camera movement is ready.
     * <p>
     * @[member="Return"] True if the action was performed, false otherwise.
     */
    public static final boolean moveCamera() {
        return getABCUtil().performRotateCamera();
    }

    /**
     * Checks the exp of the specified skill. Happens only if the time tracker for checking exp is ready.
     * <p>
     * @param skill The skill to check.
     * @[member="Return"] True if the exp was checked, false otherwise.
     */
    public static final boolean checkXp(SKILLS skill) {
        if (skill == null) {
            return false;
        }
        return getABCUtil().performXPCheck(skill);
    }

    /**
     * Picks up the mouse. Happens only if the time tracker for picking up the mouse is ready.
     * <p>
     * @[member="Return"] True if the mouse was picked up, false otherwise.
     */
    public static final boolean pickUpMouse() {
        return getABCUtil().performPickupMouse();
    }

    /**
     * Navigates the mouse off game window and mimics defocusing the window. Happens only if the time tracker for leaving the game is ready.
     * <p>
     * @[member="Return"] True if the mouse left the game window, false otherwise.
     */
    public static boolean leaveGame() {
        return getABCUtil().performLeaveGame();
    }

    /**
     * Examines a random object near your player. Happens only if the time tracker for examining a random object is ready.
     * <p>
     * @[member="Return"] True if an object was examined, false otherwise.
     */
    public static final boolean examineObject() {
        return getABCUtil().performExamineObject();
    }

    /**
     * Right clicks a random spot on the screen. Happens only if the time tracker for right clicking a random spot is ready.
     * <p>
     * @[member="Return"] True if a random spot was right clicked, false otherwise.
     */
    public static final boolean rightClick() {
        return getABCUtil().performRandomRightClick();
    }

    /**
     * Moves the mouse to a random point on the screen. Happens only if the time tracker for randomly moving the mouse is ready.
     * <p>
     * @[member="Return"] True if the mouse was moved to a random point, false otherwise.
     */
    public static final boolean mouseMovement() {
        return getABCUtil().performRandomMouseMovement();
    }

    /**
     * Opens up the combat tab and idles for a short period of time. Happens only if the time tracker for combat tab checking is ready.
     * <p>
     * @[member="Return"] True if the combat tab was checked, false otherwise.
     */
    public static final boolean combatCheck() {
        return getABCUtil().performCombatCheck();
    }

    /**
     * Opens up the equipment tab and idles for a short period of time. Happens only if the time tracker for equipment tab checking is ready.
     * <p>
     * @[member="Return"] True if the equipment tab was checked, false otherwise.
     */
    public static final boolean equipmentCheck() {
        return getABCUtil().performEquipmentCheck();
    }

    /**
     * Opens up the friends tab and idles for a short period of time. Happens only if the time tracker for friends tab checking is ready.
     * <p>
     * @[member="Return"] True if the friends tab was checked, false otherwise.
     */
    public static final boolean friendsCheck() {
        return getABCUtil().performFriendsCheck();
    }

    /**
     * Opens up the music tab and idles for a short period of time. Happens only if the time tracker for music tab checking is ready.
     * <p>
     * @[member="Return"] True if the music tab was checked, false otherwise.
     */
    public static final boolean musicCheck() {
        return getABCUtil().performMusicCheck();
    }

    /**
     * Opens up the quest tab and idles for a short period of time. Happens only if the time tracking for quest tab checking is ready.
     * <p>
     * @[member="Return"] True if the quest tab was checked, false otherwise.
     */
    public static final boolean questCheck() {
        return getABCUtil().performQuestsCheck();
    }

    /**
     * Checks all of the actions that are perform with the time tracker; if any are ready, they will be performed.
     * <p>
     * @param skill The skill to check the xp of.
     *              Null if no xp is being gained.
     * @[member="Return"] True if any actions were performed, false otherwise.
     */
    public static final boolean timedActions(SKILLS skill) {
        return getABCUtil().performTimedActions(skill);
    }

    /**
     * Sleeps the current thread for the switch object delay time.
     * <p>
     * This method should be called in between interactions. The interaction that is currently taking place should be one that has an uncertain finish time
     * such as chopping a tree, mining a rock, or fishing.
     * In example:
     * Your player has finished chopping down a tree and is ready to chop a new tree. You should call this method before you start chopping the new tree.
     * <p>
     * @[member="see"] #waitNewOrSwitchDelay(long, boolean)
     */
    public static final void waitSwitchObjectDelay() {
        General.sleep(getABCUtil().DELAY_TRACKER.SWITCH_OBJECT.next());
        getABCUtil().DELAY_TRACKER.SWITCH_OBJECT.reset();
    }

    /**
     * Sleeps the current thread for the switch object combat delay time.
     * <p>
     * This method should be called in between interactions with NPC's you are in combat with.
     * In example:
     * Your player has finished killing a cow and is ready to attack a new cow. You should call this method before you start attacking the new cow.
     * <p>
     * @[member="see"] #waitNewOrSwitchDelay(long, boolean)
     */
    public static final void waitSwitchObjectCombatDelay() {
        General.sleep(getABCUtil().DELAY_TRACKER.SWITCH_OBJECT_COMBAT.next());
        getABCUtil().DELAY_TRACKER.SWITCH_OBJECT_COMBAT.reset();
    }

    /**
     * Sleeps the current thread for the new object delay time.
     * <p>
     * This method should be called before starting an interaction with an object that has just spawned. You should only call this method if there are no other
     * available objects to interact with and your character is waiting for a new one to spawn.
     * In example:
     * Your player has chopped all of the trees down in the area your player is chopping in. You should call this method after a new tree spawns that your
     * player is going to start chopping.
     * <p>
     * @[member="see"] #waitNewOrSwitchDelay(long, boolean)
     */
    public static final void waitNewObjectDelay() {
        General.sleep(getABCUtil().DELAY_TRACKER.NEW_OBJECT.next());
        getABCUtil().DELAY_TRACKER.NEW_OBJECT.reset();
    }

    /**
     * Sleeps the current thread for the new object combat delay time.
     * <p>
     * This method should be called before starting an interaction with a npc that has just spawned. You should only call this method if there are no other
     * available npcs to interact with and your character is waiting for a new one to spawn.
     * In example:
     * Your player has killed all of the npcs in the area your player is in. You should call this method after a new npc spawns that your player is going to
     * attack.
     * <p>
     * @[member="see"] #waitNewOrSwitchDelay(long, boolean)
     */
    public static final void waitNewObjectCombatDelay() {
        General.sleep(getABCUtil().DELAY_TRACKER.NEW_OBJECT_COMBAT.next());
        getABCUtil().DELAY_TRACKER.NEW_OBJECT_COMBAT.reset();
    }

    /**
     * Sleeps the current thread for the switch/new object delay time for non-combat and combat.
     * <p>
     * This method
     * <p>
     * @param last_busy_time The timestamp at which the player was last busy (a player is considered busy if they are interacting with something). The timestamp
     *                       should be marked when the player has to move on to the next interaction.
     * <p>
     * @param combat         True if the player is in combat, or the script is one which the player is constantly performing actions and requires the player to
     *                       have very fast actions (such as the sorceress garden).
     */
    public static final void waitNewOrSwitchDelay(final long last_busy_time, final boolean combat) {
        getABCUtil().waitNewOrSwitchDelay(last_busy_time, combat);
    }

    /**
     * Sleeps the current thread for the item interaction delay time.
     * <p>
     * This method should be called directly after interacting with an item in your players inventory.
     */
    public static final void waitItemInteractionDelay() {
        General.sleep(getABCUtil().DELAY_TRACKER.ITEM_INTERACTION.next());
    }

    /**
     * Sleeps the current thread for the item interaction delay time multiplied by the specified number of iterations. This method can be used to sleep between
     * certain actions that do not have a designated method already assigned to them such as casting spells or clicking interfaces.
     * <p>
     * This method does not guarantee a static sleep time each iteration.
     * <p>
     * @param iterations How many times to sleep the item interaction delay time.
     * @[member="see"] #waitItemInteractionDelay()
     */
    public static final void waitItemInteractionDelay(int iterations) {
        for (int i = 0; i < iterations; i++) {
            General.sleep(getABCUtil().DELAY_TRACKER.ITEM_INTERACTION.next());
        }
    }

    /**
     * Eats/drinks an item in your inventory with the specified name if your current HP percent is less than or equal to {@[member="Link"] #getNextEatPercent()}.
     * <p>
     * Note that if there is any delay/lag that is longer than 3000 milliseconds between the time the food/drink was clicked and when your players HP is
     * changed the tracker will not be reset and you will have to reset it manually.
     * <p>
     * @param option The option to click the food/drink with (this is normally "Eat" or "Drink").
     *               Input an empty string to have the method attempt to find the correct option automatically. Note that this is not guaranteed to execute properly if an
     *               empty string is inputted.
     * @param name   The name of the food or drink.
     * @[member="Return"] True if the food/drink was successfully eaten/drank, false otherwise.
     * @[member="see"] #eat(java.lang.String, org.tribot.api2007.types.RSItem)
     */
    public static final boolean eat(String option, final String name) {
        if (name == null) {
            return false;
        }
        return eat(option, Inventory.find(name)[0]);
    }

    /**
     * Eats/drinks an item in your inventory with the specified ID if your current HP percent is less than or equal to {@[member="Link"] #getNextEatPercent()}.
     * <p>
     * Note that if there is any delay/lag that is longer than 3000 milliseconds between the time the food/drink was clicked and when your players HP is
     * changed the tracker will not be reset and you will have to reset it manually.
     * <p>
     * @param option The option to click the food/drink with (this is normally "Eat" or "Drink").
     *               Input an empty string to have the method attempt to find the correct option automatically. Note that this is not guaranteed to execute
     *               properly if an empty string is inputted.
     * @param id     The ID of the food or drink.
     * @[member="Return"] True if the food/drink was successfully eaten/drank, false otherwise.
     * @[member="see"] #eat(java.lang.String, org.tribot.api2007.types.RSItem)
     */
    public static final boolean eat(String option, final int id) {
        return eat(option, Inventory.find(id)[0]);
    }

    /**
     * Eats/drinks the item specified if your current HP percent is less than or equal to {@[member="Link"] #getNextEatPercent()}.
     * <p>
     * Note that if there is any delay/lag that is longer than 3000 milliseconds between the time the food/drink was clicked and when your players HP is
     * changed the tracker will not be reset and you will have to reset it manually.
     * <p>
     * @param option The option to click the food/drink with (this is normally "Eat" or "Drink").
     *               Input an empty string to have the method attempt to find the correct option automatically. Note that this is not guaranteed to execute
     *               properly if an empty string is inputted.
     * @param item   The item to eat or drink.
     * @[member="Return"] True if the food/drink was successfully eaten/drank, false otherwise.
     * @[member="see"] #getNextEatPercent()
     */
    public static final boolean eat(String option, RSItem item) {
        if (option == null || item == null) {
            return false;
        }

        final int current_hp = Combat.getHPRatio();
        final int next_hp = getNextEatPercent();

        if (current_hp <= next_hp) {
            if (option.isEmpty()) {
                String[] actions = item.getDefinition().getActions();
                for (String action : actions) {
                    if (action.contains("Eat") || action.contains("Drink")) {
                        option = action;
                        break;
                    }
                }
            }
            if (!option.isEmpty() && Clicking.click(option, item)) {
                if (Timing.waitCondition(new Condition() {
                    @Override
                    public boolean active() {
                        waitItemInteractionDelay();
                        return Combat.getHPRatio() > next_hp;
                    }
                }, 3000)) {
                    getABCUtil().INT_TRACKER.NEXT_EAT_AT.reset();
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Hovers the next available object if applicable.
     * <p>
     * Note that you <i>must</i> reset the tracker yourself after the current object interaction is finished.
     * <p>
     * @param currentlyInteracting The object you are currently interacting with.
     */
    public static final void hoverNextObject(final RSObject currentlyInteracting) {
        if (currentlyInteracting == null || !getABCUtil().BOOL_TRACKER.HOVER_NEXT.next()) {
            return;
        }

        String name = "";

        //<editor-fold defaultstate="collapsed" desc="Get name">
        RSObjectDefinition definition = currentlyInteracting.getDefinition();
        if (definition != null) {
            String definitionName = definition.getName();
            if (definitionName != null) {
                name = definitionName;
            }
        }
        if (name.isEmpty()) {
            return;
        }
        //</editor-fold>

        final String objectName = name;
        final RSObject next = Objects.find(15,new Filter<RSObject>() {
            //<editor-fold defaultstate="collapsed" desc="Filter">
            @Override
            public boolean accept(RSObject o) {
                if (o == null) {
                    return false;
                }
                final RSObjectDefinition def = o.getDefinition();
                if (def != null) {
                    final String name = def.getName();
                    if (name != null) {
                        return name.equals(objectName) && !o.getPosition().equals(currentlyInteracting.getPosition());
                    }
                }
                return false;
            }
            //</editor-fold>
        })[0];
        if (next != null) {
            General.println(Arrays.asList(next.getModel().getPoints()).contains(Mouse.getPos()));
            if (!Mouse.getPos().equals(next.getModel().getHumanHoverPoint()) && Clicking.hover(next)) {
                Timing.waitCondition(new Condition() {
                    @Override
                    public boolean active() {
                        waitItemInteractionDelay();
                        return Mouse.getPos().equals(next.getModel().getHumanHoverPoint());
                    }
                }, 500);
            }
        }
    }

    /**
     * Hovers the next available NPC if applicable.
     * <p>
     * Note that you <i>must</i> reset the tracker yourself after the current NPC interaction is finished.
     */
    public static final void hoverNextNPC() {
        if (!getABCUtil().BOOL_TRACKER.HOVER_NEXT.next()) {
            return;
        }

        final RSCharacter interacting = Player.getRSPlayer().getInteractingCharacter();

        if (interacting != null) {
            final String name = interacting.getName();
            if (name != null) {
                final RSNPC next = NPCs.find(new Filter<RSNPC>() {
                    //<editor-fold defaultstate="collapsed" desc="NPC filter">
                    @Override
                    public boolean accept(RSNPC npc) {
                        if (npc == null) {
                            return false;
                        }
                        RSNPCDefinition def = npc.getDefinition();
                        if (def == null) {
                            return false;
                        }
                        String def_name = def.getName();
                        if (def_name == null || !def_name.equals(name)) {
                            return false;
                        }
                        return npc.isOnScreen() && npc.isClickable() && !npc.getPosition().equals(interacting.getPosition());
                    }
                    //</editor-fold>
                })[0];
                if (next != null) {
                    if (!Mouse.getPos().equals(next.getModel().getHumanHoverPoint()) && Clicking.hover(next)) {
                        Timing.waitCondition(new Condition() {
                            @Override
                            public boolean active() {
                                waitItemInteractionDelay();
                                return Mouse.getPos().equals(next.getModel().getHumanHoverPoint());
                            }
                        }, 500);
                    }
                }
            }
        }
    }

    /**
     * Gets an object near you with the specified ID.
     * <p>
     * This object will either be the nearest, or second nearest depending on what the tracker returns.
     * <p>
     * @param id The ID of the object you are looking to find.
     * @[member="Return"] An object with the specified ID near you.
     *         Null if no objects were found.
     */
    public static final RSObject getObject(final int id) {
        final RSObject[] objects = Objects.find(15, id);
        Sorting.sortByDistance(objects, Player.getPosition(), true);
        if (objects.length < 1) {
            return null;
        }
        RSObject object_to_click = objects[0];
        if (objects.length > 1 && getABCUtil().BOOL_TRACKER.USE_CLOSEST.next()) {
            if (objects[1].getPosition().distanceToDouble(objects[0]) <= 3.0) {
                object_to_click = objects[1];
            }
        }
        getABCUtil().BOOL_TRACKER.USE_CLOSEST.reset();
        return object_to_click;
    }

    /**
     * Gets an object near you with the specified name.
     * <p>
     * This object will either be the nearest, or second nearest depending on the tracker.
     * <p>
     * @param name The name of the object you are looking to find.
     * @[member="Return"] An object with the specified name near you. Null if no objects were found.
     */
    public static final RSObject getObject(final String name) {
        final RSObject[] objects = Objects.find(15, name);
        Sorting.sortByDistance(objects, Player.getPosition(), true);
        if (objects.length < 1) {
            return null;
        }
        RSObject object_to_click = objects[0];
        if (objects.length > 1 && getABCUtil().BOOL_TRACKER.USE_CLOSEST.next()) {
            if (objects[1].getPosition().distanceToDouble(objects[0]) <= 3.0) {
                object_to_click = objects[1];
            }
        }
        getABCUtil().BOOL_TRACKER.USE_CLOSEST.reset();
        return object_to_click;
    }

    /**
     * Walks to the next anticipated resource.
     * <p>
     * Note that you must calculate which resource is the next anticipated resource yourself. You must also reset the tracker yourself after the current
     * resource is gone/depleted. The next anticipated resource generally can be retrieved by calling #getObject(int) or #getObject(java.lang.String).
     * <p>
     * @param anticipated The next anticipated resource.
     * @[member="Return"] True if the player moved to the resource; false otherwise.
     * @[member="see"] #getObject(java.lang.String)
     * @[member="see"] #getObject(int)
     */
    public static final boolean goToAnticipated(Positionable anticipated) {
        if (anticipated != null && getABCUtil().BOOL_TRACKER.GO_TO_ANTICIPATED.next()) {
            return Walking.walkTo(anticipated.getPosition());
        }
        return false;
    }

    /**
     * Checks to see if the player should switch resources.
     * <p>
     * Note that you must reset the tracker yourself if you switch resources successfully.
     * Note that this method will only return correctly if you have been tracking the resources you have won and lost.
     * <p>
     * @param player_count    The amount of players gathering resources near you.
     * @param last_time_moved The time stamp at which you last time you moved to a new resource.
     * @[member="Return"] True if your player should switch resources, false otherwise.
     */
    public static final boolean shouldSwitchResources(int player_count, long last_time_moved) {
        double win_percent = ((double) (resources_won + resources_lost) / (double) resources_won);
        return win_percent < 50.0 && getABCUtil().SWITCH_TRACKER.TOO_MANY_PLAYERS.next(player_count);
    }
}
