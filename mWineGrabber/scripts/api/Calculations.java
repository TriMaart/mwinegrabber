package scripts.api;

import org.tribot.api.General;
import org.tribot.api.Timing;
import org.tribot.api.types.generic.Condition;
import org.tribot.api2007.Banking;
import org.tribot.api2007.Inventory;
import org.tribot.api2007.Projection;
import org.tribot.api2007.types.RSItem;
import org.tribot.api2007.types.RSTile;
import scripts.api.types.Staff;

import java.awt.*;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Jeroen on 1-9-2015.
 */
public class Calculations {

    public static String getTimeRan(long START_TIME){
        return Timing.msToString(Timing.timeFromMark(START_TIME));

    }

    public static long getPerHour(int amount,long START_TIME){

        return Math.round(amount / (Timing.timeFromMark(START_TIME) / 3600000.0));
    }

    public static boolean supplyCheckBank(final HashMap<String, Integer> set){

        if (!Banking.isBankScreenOpen()){
            General.println("Bank is not open. Could not determine supplies. Assuming we have sufficient supplies.");
            return true;
        }

        return Timing.waitCondition(new Condition() {
            @Override
            public boolean active() {
                General.sleep(100);
                for (String item : set.keySet()) {
                    RSItem[] bankItem = Banking.find(item);
                    if (bankItem.length <= 0 || bankItem[0].getStack() < set.get(item)) {
                        return false;
                    }
                }
                return true;
            }
        }, General.random(1500, 2000));
    }

    public static boolean gotItemsInventory(final HashMap<String,Integer> set){
        for (String item : set.keySet()) {
            RSItem[] invItem = Inventory.find(item);
            if (invItem.length > 0) {
                if (invItem.length < set.get(item) && invItem[0].getStack() < set.get(item)) {
                    return false;
                }
            }
            else{
                return false;
            }
        }
        return true;
    }

    public static HashMap<String, Integer> mergeAndAdd(HashMap<String, Integer>... maps) {
        HashMap<String, Integer> result = new HashMap<>();
        for (HashMap<String, Integer> map : maps) {
            for (Map.Entry<String, Integer> entry : map.entrySet()) {
                String key = entry.getKey();
                Integer current = result.get(key);
                result.put(key, current == null ? entry.getValue() : entry.getValue() + current);
            }
        }
        return result;
    }

    public static Point randomPointRectangle(Rectangle rectangle){
        int width = (int)rectangle.getWidth();
        int heigth = (int)rectangle.getHeight();
        return new Point((int)rectangle.getMinX() + General.randomSD(0, width, width / 2, width / 7),(int)rectangle.getMinY() + General.randomSD(0, heigth, heigth / 2, heigth / 7));
    }

    public static Point centerOfATile(RSTile tile, int height){
        Point[] array = Projection.getTileBounds(tile, height);
        double dX = (array[1].getX() - array[3].getX()) / 2 ;
        double dY = (array[1].getY() - array[3].getY()) / 2;
        Point p = new Point((int)(array[3].getX()+dX) + General.random(-6,6),(int)(array[3].getY()+dY) + General.random(-6,6));
        return p;
    }
}
